<?php

namespace paml\Log\Listener;

use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\EventManager\ListenerAggregateTrait;
use Zend\Console\Request as ConsoleRequest;
use Zend\Http\Request;
use Zend\Log\Logger;
use Zend\Mvc\MvcEvent;

class ErrorListener implements ListenerAggregateInterface
{
    use ListenerAggregateTrait;

    protected $logger;

    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
    }

    public function attach(EventManagerInterface $events, $priority = 1)
    {
        $events->attach(
            MvcEvent::EVENT_DISPATCH_ERROR,
            [$this, 'onErrorOccured'],
            $priority
        );

        $events->attach(
            MvcEvent::EVENT_RENDER_ERROR,
            [$this, 'onErrorOccured'],
            $priority
        );
    }

    public function onErrorOccured(MvcEvent $event)
    {
        /**
         * @var $e \Zend\View\Model\ViewModel
         */
        $result = $event->getResult();
        $result->setTerminal(true);

        /** @var $request ConsoleRequest */
        $request = $event->getParam('request');

        if ($request instanceof ConsoleRequest) {
            $this->generateConsoleError($request);
        } elseif ($request instanceof Request) {
            $error = $event->getParam('error');
            $this->generateHttpError($request, $error);
        }

        $exception = $event->getParam('exception');

        if (!empty($exception)) {
            $this->generateException($exception);
        }

        $this->logger->err('--------------------------------------------------------------------------------' . PHP_EOL);
    }

    private function generateConsoleError(ConsoleRequest $request)
    {
        $commend = ' ';

        if ($request->getParams()->toArray()) {
            $commend .= implode(' ', $request->getParams()->toArray());
        }

        $this->logger->err('[CONSOLE] ' . $request->getScriptName() . $commend);
    }

    private function generateHttpError(Request $request, string $error = null)
    {
        $this->logger->err('[' . $request->getMethod() . ']  ' . $request->getUri());

        if ($error != 'error-router-no-match') {
            if (isset($request->getServer()['SERVER_ADDR']) and $request->getHeader('User-Agent')) {
                $this->logger->err('[' . $request->getServer()['SERVER_ADDR'] . '] ' . $request->getHeader('User-Agent')->toString());
            }

            if ($request->isPost()) {
                foreach ($request->getPost() as $key => $value) {
                    if (is_array($value)) continue;
                    $this->logger->err($key . ': ' . $value);
                }
            }
        }
    }

    private function generateException(\Throwable $exception)
    {
        $this->logger->err('--------------------------------------------------------------------------------');
        $this->logger->err($exception->getFile() . ': ' . $exception->getLine());
        $this->logger->err('[' . $exception->getCode() . '] ' . $exception->getMessage());
        $this->logger->err('Stack trace:');

        foreach ($exception->getTrace() as $trace) {
            if (isset($trace['file']) and isset($trace['line']) ) {
                $this->logger->err($trace['file'] . ': ' . $trace['line']);
            }
        }
    }
}
