<?php

namespace paml\Log;

use paml\Log\Listener\ErrorListener;
use Zend\Mvc\MvcEvent;

class Module
{
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    public function onBootstrap(MvcEvent $e)
    {
        $serviceManager = $e->getApplication()->getServiceManager();
        $eventManager = $e->getTarget()->getEventManager();

        $errorListener = $serviceManager->get(ErrorListener::class);
        $errorListener->attach($eventManager);
    }
}
