<?php

namespace paml\Log\Factory;

use paml\Log\Listener\ErrorListener;
use Interop\Container\ContainerInterface;
use Zend\Log\Writer\Stream;

class ErrorListenerFactory
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): ErrorListener
    {
        $data = (new \DateTime())->format('Ym');

        $filePath = "./data/log/error-{$data}.log";
        $streamWriter = new Stream($filePath, "a", "\n", 0777);
        $logger = $container->get('jhu.zdt_logger')->addWriter($streamWriter);

        return new ErrorListener($logger);
    }
}
