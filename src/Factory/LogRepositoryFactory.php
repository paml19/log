<?php

namespace paml\Log\Factory;

use paml\Log\Model\LogHydrator;
use paml\Log\Model\LogRepository;
use Interop\Container\ContainerInterface;
use Zend\Db\Adapter\AdapterInterface;

class LogRepositoryFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $dbAdapter = $container->get(AdapterInterface::class);
        $logHydrator = $container->get(LogHydrator::class);

        return new LogRepository($dbAdapter, $logHydrator);
    }
}
