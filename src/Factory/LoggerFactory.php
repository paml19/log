<?php

namespace paml\Log\Factory;

use Interop\Container\ContainerInterface;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Log\Logger;
use Zend\Log\Processor\PsrPlaceholder;
use Zend\Log\Writer\Db;

class LoggerFactory
{
    protected $mapping = [
        'timestamp' => 'date',
        'priority'  => 'type',
        'message'   => 'content'
    ];

    public function __invoke(ContainerInterface $container)
    {
        $dbAdapter = $container->get('dbLocal');
        $psrPlaceholder = new PsrPlaceholder();

        $writer = (new Db($dbAdapter, 'system_log', $this->mapping))
            ->setFormatter(new \Zend\Log\Formatter\Db('Y-m-d H:i:s'));

        $logger = (new Logger())
            ->addWriter($writer)
            ->addProcessor($psrPlaceholder);

        return $logger;
    }
}
